#
# Endless loop that queries tc stats and logs them to a file
# every $SLEEPTIME seconds
# Note: Needs right to invoke tc


IFACES=("$1")
DURATION=$2
TC_COMPONENTS=("qdisc") #"class") # "filter")

TC=tc
IPTABLES=iptables
if [ "$USER" != "root" ] 
	then
	TC="/sbin/tc"
	#IPTABLES="sudo /sbin/iptables"
fi

#rm log/*.log
rm  /tmp/tc_*.log

start_time=$(date +%s%N)
end_time=$((start_time + DURATION * 1000000000))

while true;
do
	for iface in ${IFACES[@]};
	do
		timestamp=$(date +%s%N)
		for tccomp in ${TC_COMPONENTS[@]};
		do
			echo -n "$timestamp "  >> /tmp/tc_${tccomp}_${iface}.log
			$TC -s ${tccomp} show dev $iface | grep "backlog" | tail -1 | awk '{print $3}'  >> /tmp/tc_${tccomp}_${iface}.log
		done
	done
        if [ "$timestamp" -ge "$end_time" ]; then
      	  break 
        fi
	sleep 0.001
done
