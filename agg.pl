#! /usr/bin/perl
 $timeint = shift;
#$timeint = 0.05;
if (defined($opt_t)) {
    $timeint = $opt_t;
}

$_ = <>;
($time, $data, $gput_data) = split;
print "$time $data \n";
$sum = 1;
$agg = $data;
$agg_gput = $gput_data;

$timestart = int($time / $timeint) * $timeint;
$timenext = $timestart + $timeint;

while (<>) {
    ($time, $data, $gput_data) = split;
    while ($time > $timenext) {
        $timeprev = $timenext - $timeint;
        #print "$timeprev $sum $agg\n";
        print "$timeprev $agg $agg_gput $sum\n";
        $timenext += $timeint;
        # print "    erhoehe timenext: $timenext\n";
        $sum = 0;
        $agg = 0;
	$agg_gput = 0;
    }
    $sum++;
    $agg += $data;
    $agg_gput += $gput_data;
    # print "next $time $sum  $timenext\n";
}
$timeprev = $timenext - $timeint;
print "$timeprev $agg $agg_gput $sum\n";

