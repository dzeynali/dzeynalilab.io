#!/bin/bash

source ./variables.sh
source ./getters.sh
source ./setup_servers.sh


# Setup bridges and disable TSO. Add routing to this function if necessary
preparing_servers

setup_all (){
  local cca=$1
  local rate=$2
  local queue=$3
  local delay_factor=$4
  local repeat=$5

  local cca1=$(echo "$cca" | cut -d ':' -f1)
  local cca2=$(echo "$cca" | cut -d ':' -f2)
  local cca3=$(echo "$cca" | cut -d ':' -f3)

  ########################################SUMMARY OF THE EXPERIMENT###################################################
  echo -e  "\t<<<<<--------------------RUN ($repeat of $REPEATS)---------------------->>>>>
           --> CCA1:$cca1 CCA2:$cca2 CCA3:$cca3
           --> shaping:$rate bps $(($rate/1000000)) Mbps
           --> queue_size=${queue}Bytes $((${queue}/1000)) Kbytes"
  print_delays $delay_factor
  echo -e "\t<<<<<---------------------------------------------------->>>>>"
  ####################################################################################################################

  set_multiple_tc_delay NLE1 $delay_factor
  set_multiple_tc_delay NLE2 $delay_factor
  set_shaping_rate $rate $queue

  set_cc srv-L1 $cca1
  set_cc srv-R1 $cca1

  set_cc srv-L2 $cca2
  set_cc srv-R2 $cca2

  set_cc srv-L3 $cca3
  set_cc srv-R3 $cca3
}

run_all (){
  repeat=1
  while [ $repeat -le $REPEATS ]; do
    for cc in "${CONGESTIONS[@]}"; do
      for delay_factor in "${DELAY_FACTORS[@]}"; do
        for shaping in "${SHAPINGS[@]}"; do
          for queue in "${QUEUES[@]}"; do
              rate=$(get_bps $shaping $SHAPING_UNIT)
              queue_size=$((queue*1000))
              setup_all $cc $rate $queue_size $delay_factor $repeat
              file_name=$(get_filename $cc $rate $queue_size)
              bash run_experiment.sh $file_name $delay_factor
            done
          done
        done
    done
    repeat=$((repeat+1))
  done
}

run_all