#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Plot Throughput
"""
import sys, os
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager
from matplotlib.ticker import AutoMinorLocator
import argparse
import glob, re
import numpy as np
import seaborn as sns
import base_plotter as bp


def plot_bfct(files):
    bp.__config_base_sns_style()
    fig, ax = plt.subplots(figsize=bp.get_dim())
    bp.set_style(ax)

    df_lst=[]
    for fl in sorted(files):
        print("filename: {}".format(fl))
        df = pd.read_csv(fl, header=0, delimiter=' ')
        df_lst.append(df)

    df_res=pd.concat(df_lst, ignore_index=True)

    g = sns.boxplot(data=df_res, x="bin", y="duration", hue='cca') #, palette=bp.col_dict)

    g.set_yscale("log")
    g.set_ylabel("FCT(s)")
    g.set_xlabel("Flow size (Bytes)")

    ax.tick_params(axis='x') #, rotation=30)

    bp.set_style(ax)
    ax.grid(visible='False', axis='y', which='major')
    plt.legend(loc='upper left', bbox_to_anchor=(0.0, 1.01), ncol=1)
    plt.gca().spines['bottom'].set_visible(False)
    filename="bfct.pdf"
    #plt.title("CCA: {}, Bottleneck: {}{}, QSize: {}KB".format(cc_name, shaper_rate,str(shaper_suffix), qsize),weight='bold')
    pdf_fname = 'binned-fct.pdf'
    print(pdf_fname)
    fig.savefig(str(pdf_fname), format="pdf", bbox_inches="tight")
    
if __name__ == '__main__':
    bfct_files = glob.glob('*.bfct')
    plot_bfct(bfct_files)
