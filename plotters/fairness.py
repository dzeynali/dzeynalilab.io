#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Plot Throughput
"""
import sys, os
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager
from matplotlib.ticker import AutoMinorLocator
import argparse
import glob, re
import numpy as np
import seaborn as sns
import base_plotter as bp

def plot_fairness(files):
    bp.__config_base_style()

    fig, ax = plt.subplots(figsize=bp.get_dim())
    bp.set_style(ax)

    i,s,r=(0 for i in range(3))
    lns,lns_ncf,lns_recv,ax_ln,cca_lst = ([] for i in range(5))

    for fl in sorted(files): #, reverse=True):
        cc_name = fl.split('.')[0]
        tput_data = pd.read_csv(fl, header=None, delimiter=' ')
        lns_recv.append(ax.plot(tput_data[0], tput_data[1], lw=2, zorder=500,linestyle=bp.line_dict[bp.cca_dict[cc_name]],
                      label=bp.cca_dict[cc_name], color=bp.col_dict[bp.cca_dict[cc_name]])) #linestyle=bp.line_dict[cc_name], , marker= bp.marker_dict[bp.cca_dict[cc_name]]
        ax_ln.append(tput_data[0].values[-1])
        cca_lst.append(cc_name)

    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Jain\'s Fairness Index')
    plt.minorticks_on()
    ax.legend(ncol=2)



    pdf_fname = cc_name+'.pdf'
    print(pdf_fname)
    fig.savefig(str(pdf_fname), format="pdf", bbox_inches="tight")



def plot_cdf(files, zoom):
    ###### Plot FCT CDF ######################
    bp.__config_base_style()

    fig, ax = plt.subplots(figsize=bp.get_dim())
    bp.set_style(ax)

    for fl in files:
        cca = bp.get_cc_name(fl)
        shaper_rate, shaper_suffix, delay, delay_suffix, qsize, cthresh, timenow, datenow, y_queue = bp.get_file_props(fl)
        print (cca)
        df = pd.read_csv(fl, header=None, delimiter=' ')
        x = np.sort(df[1]) #2
        y = np.arange(1, len(x)+1)/len(x)
        ax.plot(x, y, color=bp.col_dict[cca], linestyle=bp.line_dict[cca], lw=2.5, zorder=500, label=cca) #marker= bp.marker_dict[cca],markevery=int(1000/int(delay)),ms=8

    shaper_rate, shaper_suffix, delay, delay_suffix, qsize, cthresh, timenow, datenow, y_queue = bp.get_file_props(fl)
    #ax.set_xscale('log')
    ax.set_xlabel("Jain\'s Fairness Index")
    ax.set_ylabel("CDF")
    ax.set_xlim(0.3,1)
    ax.set_ylim(None,1)
    rtt = int(delay)*2

    major_locator = ticker.MultipleLocator(0.2)
    ax.yaxis.set_major_locator(major_locator)

    minor_locator_y = ticker.MultipleLocator(0.1)
    ax.yaxis.set_minor_locator(minor_locator_y)

    minor_locator_x = ticker.MultipleLocator(0.05)
    ax.xaxis.set_minor_locator(minor_locator_x)


    handles, labels = plt.gca().get_legend_handles_labels()
    order = [1,0,3,2]
    plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc='upper center', ncol=2)

    filename =  '4steps-'+str(rtt)+"msRTT-fairness-CDF.pdf"
    fig.savefig(str(filename), format="pdf", bbox_inches="tight")



if __name__ == '__main__':
    fput_files = glob.glob('*.fput')
    plot_fairness(fput_files)