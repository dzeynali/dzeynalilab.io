#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Prepare plot base so they all follow same format.
""" 

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
import numpy as np
import sys
import seaborn as sns
import re


BASE_SZ = 20
TEXT_COLOR = '#202020'

ZOOM=0

GRAN_FACTOR=16 #8*delay = 4 RTTs

col_dict = {'BBRv3':'#4daf4a','BBRv2':'#e41a1c','DCTCP':'#4daf4a',
           'Cubic':'#377eb8','NewReno':'#ff7f00','Vegas':'#a65628',
           'HTCP':'#984ea3','bbrv3':'#4daf4a','bbrv2':'#e41a1c','dctcp':'#4daf4a',
           'cubic':'#377eb8','reno':'#ff7f00','newreno':'#ff7f00','vegas':'#a65628',
           'htcp':'#984ea3', 'BBRv1':'#8856a7', 'bbrv1':'#8856a7'}

marker_dict = {'BBRv3':'*','BBRv2':'p','DCTCP':'d',
           'Cubic':'o','NewReno':'x','Vegas':'v',
           'HTCP':'+','bbrv3':'*','bbrv2':'p','dctcp':'d',
           'cubic':'o','reno':'x','newreno':'x','vegas':'v',
           'htcp':'+', 'BBRv1':'v', 'bbrv1':'v'}

line_dict = {'BBRv3':'-','BBRv2':'-.','Cubic':'--','NewReno':'-.','bbrv3':'-',
           'cubic':'--','reno':'-.','newreno':'-', 'BBRv1':':', 'bbrv1':':'}

hatch_dict = {'BBRv3':'xx','BBRv2':'++.','Cubic':'//','BBRv2':'\\','bbrv3':'xx',
           'cubic':'//','reno':'\\','BBRv2':'\\', 'BBrv1':'..', 'bbrv1':'..'}

cca_dict = {'Cubic':'Cubic', 'cubic':'Cubic',
            'NewReno':'NewReno','reno':'NewReno','newreno':'NewReno',
            'BBR1':'BBRv1', 'bbr1':'BBRv1',
            'BBR2':'BBRv2', 'bbr2':'BBRv2',
            'BBR3':'BBRv3', 'bbr3':'BBRv3'}

rtt_col_dict = {'10ms':'#4daf4a','20ms':'#e41a1c','40ms':'#4daf4a',
           '80ms':'#377eb8','160ms':'#ff7f00','others':'#8856a7'}


line_width = {'BBRv3':'2','BBRV2':'1.8','DCTCP':'1.4',
           'Cubic':'1.4','NewReno':'1','Vegas':'1',
           'BBRv1':'1','dctcp':'1.4',
           'cubic':'1.4','reno':'1','newreno':'1','vegas':'1',
           'bbrv1':'1'}

col_dict_m_e = {'e3':'#4daf4a','e1':'#e41a1c','e2':'#377eb8','e4':'#8856a7',}
line_dict_m_e = {'e3':'--','e1':'-','e2':'-.','e4':':'}


col_pal= ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#a65628','#f781bf','#999999']
markers = ["x","o","+","D","*","s","v","^"]
lines = ["-","-.","--","dotted","-"]
cmap = plt.cm.turbo(np.linspace(0.0, 6.0, 1000))


def __config_base_style(base_size=BASE_SZ):
    """Customize plot aesthetics."""
    mpl.rcParams['font.family'] = 'sans-serif'
    # Use a sensible *free* font.
    mpl.rcParams['font.sans-serif'] = 'Clear Sans'
    mpl.rcParams['font.size'] = base_size
    mpl.rcParams['pdf.fonttype'] = 42

    mpl.rcParams['axes.spines.top'] = False
    mpl.rcParams['axes.spines.right'] = False

    mpl.rcParams['axes.grid'] = True
    mpl.rcParams['axes.grid.which'] = 'both'
    mpl.rcParams['axes.grid.axis'] = 'both'
    mpl.rcParams['grid.linestyle'] = 'dotted'
    mpl.rcParams['grid.color'] = '#BEBEBE' 

    mpl.rcParams['axes.edgecolor'] = '#202020'
    mpl.rcParams['axes.linewidth'] = 2.0
    mpl.rcParams['axes.titlesize'] = base_size
    mpl.rcParams['axes.labelsize'] = base_size
    mpl.rcParams['axes.labelweight'] = 'bold'

    mpl.rcParams['xtick.labelsize'] = base_size
    mpl.rcParams['xtick.color'] = '#606060'
    mpl.rcParams['xtick.major.size'] = 10
    mpl.rcParams['xtick.major.width'] = 1.5
    mpl.rcParams['xtick.minor.size'] = 6
    mpl.rcParams['xtick.minor.width'] = 1.5
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['xtick.minor.pad'] = 8

    mpl.rcParams['ytick.labelsize'] = base_size
    mpl.rcParams['ytick.color'] = '#606060'
    mpl.rcParams['ytick.major.size'] = 10
    mpl.rcParams['ytick.major.width'] = 1.5
    mpl.rcParams['ytick.minor.size'] = 6
    mpl.rcParams['ytick.minor.width'] = 1.5
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['ytick.major.pad'] = 8
    mpl.rcParams['ytick.minor.pad'] = 8

    mpl.rcParams['legend.fontsize'] = base_size
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['legend.facecolor'] = 'white'
    mpl.rcParams['legend.framealpha'] = 0

def __config_base_sns_style(base_size=BASE_SZ):
    """Customize plot aesthetics."""
    sns.mpl.rcParams['font.family'] = 'sans-serif'
    # Use a sensible *free* font.
    sns.mpl.rcParams['font.sans-serif'] = 'Clear Sans'
    sns.mpl.rcParams['font.size'] = base_size
    sns.mpl.rcParams['pdf.fonttype'] = 42

    sns.mpl.rcParams['axes.spines.top'] = False
    sns.mpl.rcParams['axes.spines.right'] = False

    sns.mpl.rcParams['axes.grid'] = True
    sns.mpl.rcParams['axes.grid.axis'] = 'both'
    sns.mpl.rcParams['axes.grid.which'] = 'both'
    sns.mpl.rcParams['grid.linestyle'] = 'dotted'
    sns.mpl.rcParams['grid.color'] = '#BEBEBE'

    mpl.rcParams['axes.edgecolor'] = '#202020'
    mpl.rcParams['axes.linewidth'] = 2.0
    mpl.rcParams['axes.titlesize'] = base_size
    mpl.rcParams['axes.labelsize'] = base_size
    mpl.rcParams['axes.labelweight'] = 'bold'

    sns.mpl.rcParams['xtick.labelsize'] = base_size
    sns.mpl.rcParams['xtick.color'] = '#606060'
    sns.mpl.rcParams['xtick.major.size'] = 10
    sns.mpl.rcParams['xtick.major.width'] = 1.5
    sns.mpl.rcParams['xtick.minor.size'] = 6
    sns.mpl.rcParams['xtick.minor.width'] = 1.5
    sns.mpl.rcParams['xtick.direction'] = 'in'
    sns.mpl.rcParams['xtick.major.pad'] = 8
    sns.mpl.rcParams['xtick.minor.pad'] = 8

    sns.mpl.rcParams['ytick.labelsize'] = base_size
    sns.mpl.rcParams['ytick.color'] = '#606060'
    sns.mpl.rcParams['ytick.major.size'] = 10
    sns.mpl.rcParams['ytick.major.width'] = 1.5
    sns.mpl.rcParams['ytick.minor.size'] = 6
    sns.mpl.rcParams['ytick.minor.width'] = 1.5
    sns.mpl.rcParams['ytick.direction'] = 'in'
    sns.mpl.rcParams['ytick.major.pad'] = 8
    sns.mpl.rcParams['ytick.minor.pad'] = 8

    sns.mpl.rcParams['legend.fontsize'] = base_size
    sns.mpl.rcParams['legend.frameon'] = True
    sns.mpl.rcParams['legend.framealpha'] = 0
    sns.mpl.rcParams['legend.facecolor'] = 'white'


def get_axes_font(base_size=BASE_SZ, color=TEXT_COLOR):
    return {'fontsize': base_size, 'fontweight': 'medium', 'color': color}


def get_dim(**kwargs):
    ratio = 1.67
    if 'height' in kwargs:
        height = kwargs['height']
    else:
        height = 4

    if 'width' in kwargs:
        width = kwargs['width']
        if not height:
            height = width / ratio
    else:
        width = height * ratio

    return width, height


def set_style(ax, text_color=TEXT_COLOR):
    map(lambda v: v.set_color(text_color), ax.get_xticklabels())
    map(lambda v: v.set_color(text_color), ax.get_yticklabels())

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.2),
              facecolor='white')


def get_gran(filename):
    if "ms_" in filename:
        gran = float(re.split(r'ms_', filename)[0].split('_')[-1])*GRAN_FACTOR/1000
    elif "us_" in filename:
        #gran = float(re.split(r'us_', filename)[0].split('_')[-1])*GRAN_FACTOR/1000000
        gran = 0.05
    return gran


def get_file_props(filename):
    shaper_rate = re.split(r'Mbps_|Gbps_', filename)[0].split('_')[-1]
    shaper_suffix = re.search(r'Mbps_|Gbps_',filename)[0].split('_')[0]
    delay = re.split(r'ms_|us_', filename)[0].split('_')[-1]
    delay_suffix = re.search(r'ms_|us_',filename)[0].split('_')[0]
    qsize = re.split(r'KB_', filename)[0].split('_')[-1]
    cthresh = re.split(r'Th_', filename)[0].split('_')[-1]
    timenow = re.split(r'.pdf', filename)[0].split('_')[-3]
    datenow = re.split(r'.pdf', filename)[0].split('_')[-4]
    y_queue = int(re.split(r'M|K|B|G',qsize)[0])

    return shaper_rate, shaper_suffix, delay, delay_suffix, qsize, cthresh, timenow, datenow, y_queue 


def get_super(x):
    normal = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-=()"
    super_s = "ᴬᴮᶜᴰᴱᶠᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾQᴿˢᵀᵁⱽᵂˣʸᶻᵃᵇᶜᵈᵉᶠᵍʰᶦʲᵏˡᵐⁿᵒᵖ۹ʳˢᵗᵘᵛʷˣʸᶻ⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻⁼⁽⁾"
    res = x.maketrans(''.join(normal), ''.join(super_s))
    return x.translate(res)


def get_sub(x):
    normal = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-=()"
    sub_s = "ₐ₈CDₑբGₕᵢⱼₖₗₘₙₒₚQᵣₛₜᵤᵥwₓᵧZₐ♭꜀ᑯₑբ₉ₕᵢⱼₖₗₘₙₒₚ૧ᵣₛₜᵤᵥwₓᵧ₂₀₁₂₃₄₅₆₇₈₉₊₋₌₍₎"
    res = x.maketrans(''.join(normal), ''.join(sub_s))
    return x.translate(res)


def getGran(variablesFile):
    gran = 0
    try:
       gran = next(line.split('=')[1].strip() for line in open(variablesFile) if "GRAN=" in line)
    except (StopIteration, ValueError):
        return None
    return gran

GRAN=getGran("../variables.sh")
