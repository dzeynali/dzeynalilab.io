import numpy as np
import matplotlib.pyplot as plt

# Read the data from your one-column values file
file_path = 'flows_size.txt'
data = np.loadtxt(file_path)

# Sort the data in ascending order
data_sorted = np.sort(data)

# Calculate the CDF
cdf = np.arange(1, len(data_sorted) + 1) / len(data_sorted)

# Create a plot
plt.plot(data_sorted, cdf, linestyle='-', color='b')

plt.xscale('log')

# Customize the plot labels and title
plt.xlabel('Flow Size (Bytes)')
plt.ylabel('CDF')

# Show the plot
plt.grid(True)

plt.savefig('cdf_flow_size.pdf', format='pdf')
