#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Plot Throughput
"""
import sys, os
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.font_manager
from matplotlib.ticker import AutoMinorLocator
import argparse
import glob, re
import numpy as np
import seaborn as sns
import base_plotter as bp

rttf1 = 0
rttf2 = 0

def plot_tput(files):
    bp.__config_base_style()

    fig, ax = plt.subplots(figsize=bp.get_dim())
    bp.set_style(ax)

    lns,lns_ncf,lns_recv,ax_ln,cca_lst = ([] for i in range(5))
    i=0
    for fl in sorted(files):
        filename = fl.split('.')[0]
        if "receiver" in fl:
            if filename.count('_') == 6:
                flowID=filename.split('_')[0]
                flowNum=flowID[flowID.rfind("flow") + len("flow"):]
                parallel_num=filename.split('_')[1]
    
                cc_name=filename.split('_')[2]
                rtt=filename.split('_')[3]
                tput_data = pd.read_csv(fl, header=None, delimiter=' ')
                leg = 'e'+bp.get_sub(str(i))+':'+bp.cca_dict[cc_name]
                lns_recv.append(ax.plot(tput_data[0], tput_data[1]*8/float(bp.GRAN)/1000/1000,
                    label=leg, linestyle=bp.line_dict[bp.cca_dict[cc_name]])) #, color=bp.col_dict_m_e[cc_name]))
                ax_ln.append(tput_data[0].values[-1])
                cca_lst.append(cc_name)
            else:
                flowID=filename.split('_')[0]    
                cc_name=filename.split('_')[1]
                rtt=filename.split('_')[2]
                tput_data = pd.read_csv(fl, header=None, delimiter=' ')
                leg = 'e'+bp.get_sub(str(i))+':'+bp.cca_dict[cc_name]
                lns_recv.append(ax.plot(tput_data[0], tput_data[1]*8/float(bp.GRAN)/1000/1000,
                    label=leg, linestyle=bp.line_dict[bp.cca_dict[cc_name]])) #, color=bp.col_dict_m_e[cc_name]))
                ax_ln.append(tput_data[0].values[-1])
                cca_lst.append(cc_name)
            i+=1

    
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Throughput (Mbps)')
    ax.legend(ncol=2)
    plt.minorticks_on()

    pdf_fname = 'throughput.pdf'
    print(pdf_fname)
    fig.savefig(str(pdf_fname), format="pdf", bbox_inches="tight")

    
if __name__ == '__main__':
    tput_files = glob.glob('*.tput')
    plot_tput(tput_files)
