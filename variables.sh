HOME_DIR=/home/bbrEval/BBRv3Eval

declare -A cca_map
cca_map["bbr1"]="BBRv1"
cca_map["bbr2"]="BBRv2"
cca_map["bbr3"]="BBRv3"
cca_map["cubic"]="Cubic"


REPEATS=1 # How many time you want to repeat the experiment?
GRAN=0.1 # Granularity for the plotters

DURATION=360 # Total duration of the experiment in seconds
START_GAP=0 # Gap to start the flows
PARALLEL=1 # Number of parallel flows

##################### for stairs experiment ##################
FLOW_COUNT=5 #  number of flows
GAP=100 #  GAP between the start of the flows. total duration would be ((FLOW_COUNT*2)-1)*GAP e.g. for 5 flows and 100s Gap = 900s
###############################################################

##################### for matrix experiment ##################
# In this experiment 1-5 flows of the "1" will compte with 1-5 flows of the "2"
matrix_dict["2"]="5" 
matrix_dict["1"]="5"
###############################################################

##################### for Harpoon experiment ##################
SESSIONS=1
DISTRIBUTION="mawi_1G_5Times_half" 
BIN_COUNT=5 # number of bins for FCT bin plotter
SEED=15 # Harpoon random seed
###############################################################

# In this part you chose which pairs of (sender,reciever) should have flows

FLOWS=(1 2 3 4 5 6 7 8 9) # flows in this list will have traffic based on the following table
#+-----------------+-----------------+---------+--------------------------------------+
#|        SRC      |        DST      | FLOW_ID |                   RTT                |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.1   |  yyy.yyy.yyy.1  |    1    |  Left_delay_1 + Right_delay_1        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.2   |  yyy.yyy.yyy.1  |    2    |  Left_delay_2 + Right_delay_1        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.3   |  yyy.yyy.yyy.1  |    3    |  Left_delay_3 + Right_delay_1        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.1   |  yyy.yyy.yyy.2  |    4    |  Left_delay_1 + Right_delay_2        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.2   |  yyy.yyy.yyy.2  |    5    |  Left_delay_2 + Right_delay_2        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.3   |  yyy.yyy.yyy.2  |    6    |  Left_delay_3 + Right_delay_2        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.1   |  yyy.yyy.yyy.3  |    7    |  Left_delay_1 + Right_delay_3        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.2   |  yyy.yyy.yyy.3  |    8    |  Left_delay_2 + Right_delay_3        |
#+-----------------+-----------------+---------+--------------------------------------+
#| xxx.xxx.xxx.3   |  yyy.yyy.yyy.3  |    9    |  Left_delay_3 + Right_delay_3        |
#+-----------------+-----------------+---------+--------------------------------------+


# We have configured 9 different RTTs that 

DELAY_FACTORS=(1) # following delays will be multiplied by these numbers (each delay_factor has its own experiment)
LEFT_DELAYS=(24 24 24) # (Left_delay_1 Left_delay_2 Left_delay_3)  This is setting up the delays on the left NLE
RIGHT_DELAYS=(25 25 25) # (Right_delay_1 Right_delay_2 Right_delay_3) This is setting up the delays on the right NLE

# options for CCA1 -> bbr3 bbr1 cubic reno bic cdg  dctcp westwood highspeed hybla htcp vegas nv veno scalable lp yeah illinois
# options for CCA2 -> bbr2 bbr1 cubic reno bic cdg  dctcp westwood highspeed hybla htcp vegas nv veno scalable lp yeah illinois
# options for CCA3 -> bbr2 bbr1 cubic reno bic cdg  dctcp westwood highspeed hybla htcp vegas nv veno scalable lp yeah illinois
CONGESTIONS=("CCA1:CCA2:CCA3") # this depends on number of the experiments and what flows you want to run 
                               # e.g (bbr3:bbr2:bbr1) 
                               # will set the BBRv3 on server xxx.xxx.xxx.1 and yyy.yyy.yyy.1
                               # will set the BBRv2 on server xxx.xxx.xxx.2 and yyy.yyy.yyy.2
                               # will set the BBRv1 on server xxx.xxx.xxx.3 and yyy.yyy.yyy.3


SHAPING_UNIT=Mbps # Mbps or Gbps
SHAPINGS=(100)  # 500 =  500Mbps or 500Gbps depandeing on the prev. variable

QUEUES=(32 125 500 2000) # Will run experiment one time per queue size. Fixed queue size in KBytes. 70Mbytes is the max queue usage we could reach





