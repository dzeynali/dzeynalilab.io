#######################################
# Get shaping rate in bits per second
# Arguments:
#   $1 shaping
#   $2 Shaping Unit (Mbps/Gbps)
# Outputs:
#   rate in bps
#######################################
get_bps () {
  local SHAPING=$1
  local SHAPING_UNIT=$2
  local bps=0

  if [[ ${SHAPING_UNIT} == *"Mbps"* ]]; then
    bps=$(($SHAPING*1000000)); # (delay * 2) /1000 * XMbps / 1000000  / 8 (Bytes)  -> (in Bytes)
  elif [[ ${SHAPING_UNIT} == *"Gbps"* ]]; then
    bps=$(($SHAPING*1000000000)); # (delay * 2)/1000 * XMbps / 1000000000 / 8 (Bytes)  -> (in Bytes)
  fi
  echo $bps
}


#######################################
# Get bandwidth delay product.
# Arguments:
#   $1 Bandwidth
#   $2 Bandwidth Unit (Mbps/Gbps)
#   $3 Delay in ms
# Outputs:
#   BDP
#######################################
get_BDP () {
  local SHAPING=$1
  local SHAPING_UNIT=$2
  local delay=$3
  local BDP=0

  if [[ ${SHAPING_UNIT} == *"Mbps"* ]]; then
    BDP=$(($delay*2*$SHAPING*1000/8)); # (delay * 2) /1000 * XMbps / 1000000  / 8 (Bytes)  -> (in Bytes)
  elif [[ ${SHAPING_UNIT} == *"Gbps"* ]]; then
    BDP=$(($delay*2*$SHAPING*1000000/8)); # (delay * 2)/1000 * XMbps / 1000000000 / 8 (Bytes)  -> (in Bytes)
  fi
  echo $BDP
}


#######################################
# Get queue size
# Arguments:
#   $1 BDP
#   $2 Queue Factor
# Outputs:
#   queue_size
#######################################
get_queue_size () {
  local temp=$(echo ''"${1}"'*'"${2}"'' | bc -l)
  local queue_size=${temp%.*}
  echo $queue_size
}


#######################################
# Get filename
# Arguments:
# Outputs:
#   filename
#######################################

get_filename (){
  local cca=$1
  local rate=$2
  local total_queue_size=$3
  local cca1=$(echo "$cca" | cut -d ':' -f1)
  local cca2=$(echo "$cca" | cut -d ':' -f2)
  local cca3=$(echo "$cca" | cut -d ':' -f3)
  
  echo "${cca1}_${cca2}_${cca3}_$((${rate}/1000000))Mbps_$((${total_queue_size}/1000))KB"

}




############################NOT USED IN BENCHMARKING PROJECT##############################

#######################################
# Get elephant or mice queue size
# Arguments:
#   $1 total Queue size
#   $2 Queue share
# Outputs:
#   elephant or mice queue_size in cells
#######################################

get_el_mi_queue_size () {
  local temp=$(echo ''"${1%.*}"'*'"${2}"'' | bc -l)
  local q_size_cells=${temp%.*}
  echo $q_size_cells
}


