
SHAPING=100
SHAPING_UNIT=Mbps
RTT=10
x=(0.25 0.5 1 2 4 8 16 32)


if [[ ${SHAPING_UNIT} == *"Mbps"* ]]; then
  BDP=$(($RTT*$SHAPING*1000/8)); # RTT(ms)  * XMbps / 1000000  / 8 (Bytes)  -> (in Bytes)
elif [[ ${SHAPING_UNIT} == *"Gbps"* ]]; then
  BDP=$(($RTT*$SHAPING*1000000/8)); # RTT(ms) * XGbps / 1000000000 / 8 (Bytes)  -> (in Bytes)
fi

queue=0



for i in "${x[@]}"; do

  queue=$(echo "scale=2; ($i * $BDP / 1000)+0.99" | bc -l)
  queue_ceil=$(echo "scale=0; $queue / 1" | bc)
  echo -n "$queue_ceil "

done
echo "in Kbytes"

for i in "${x[@]}"; do

  echo -n "${i}xBDP "

done
echo ""

for i in "${x[@]}"; do

  queue=$(echo "scale=2; ($i * $BDP / 1000)+0.99" | bc -l)
  queue_ceil=$(echo "scale=0; $queue / 1" | bc)
  echo -n "$queue_ceil(${i}xBDP) "

done
echo "in Kbytes for ${SHAPING}${SHAPING_UNIT} and ${RTT}ms RTT"


