#!/bin/bash

SSH="ssh -F ssh/config"
source ./variables.sh

#######################################
# To setup the servers
#######################################

preparing_servers () {

  bridge_interfaces NLE-L ens1 ens2
  bridge_interfaces NLE-R ens1 ens2
  bridge_interfaces btlnck ens1 ens2

  echo -e "\n\t<<<<<-----Disabling TSO------>>>>>\n"

  disable_TSO NLE-L ens1
  disable_TSO NLE-L ens2
  disable_TSO NLE-R ens1
  disable_TSO NLE-R ens2
  disable_TSO btlnck ens1
  disable_TSO btlnck ens2

  disable_TSO srv-L1 ens1
  disable_TSO srv-L2 ens1
  disable_TSO srv-L3 ens1
  disable_TSO srv-R1 ens1
  disable_TSO srv-R2 ens1
  disable_TSO srv-R3 ens1


}

#######################################
# To setup the servers congestion control.
# Globals:
#   SRV_ID
# Arguments:
#   $1 server_ID
#   $2 CCA
#######################################

set_cc () {
  server_ID=$1
  cca=$2
  echo -e "\n\t<<<<<-----Setting $server_ID CCA to $cca------>>>>>\n"
  $SSH $server_ID "sudo sysctl -w net.ipv4.tcp_congestion_control=${cca};"

}

#######################################
# To bridge the interfaces
# Arguments:
#   $1 server_ID
#   $2 interface 1
#   $3 interface 2

#######################################

bridge_interfaces () {
  server_ID=$1
  if_1=$2
  if_2=$3
  echo -e "\n\t<<<<<-----bridging $if_1 and $if_2 on $server_ID------>>>>>\n"

  $SSH $server_ID "sudo ifconfig br0 down;
                   sudo brctl delbr br0;
                   sudo ifconfig ${if_1} 0.0.0.0 down;
                   sudo ifconfig ${if_2} 0.0.0.0 down;
                   sudo brctl addbr br0;
                   sudo brctl addif br0 ${if_1};
                   sudo brctl addif br0 ${if_2};
                   sudo ifconfig br0 up;
                   sudo ifconfig ${if_1} up;
                   sudo ifconfig ${if_2} up; "
}



#######################################
# To set the delay with TC
# It will read the LEFT and RIGHT delays from the variables.sh file
# Arguments:
#   $1 server_ID
#######################################

set_multiple_tc_delay () {
  local server_ID=$1
  local delay_factor=$2
  
  NEW_LEFT_DELAYS=()
  NEW_RIGHT_DELAYS=()

  for ((i=0; i<${#LEFT_DELAYS[@]}; i++)); do
    NEW_LEFT_DELAYS+=($((LEFT_DELAYS[i] * delay_factor)))
  done
  for ((i=0; i<${#RIGHT_DELAYS[@]}; i++)); do
    NEW_RIGHT_DELAYS+=($((RIGHT_DELAYS[i] * delay_factor)))
  done

/
echo "${A[@]}"
  if [[ $server_ID == "NLE-L" ]] ; then
    echo -e "\n\t<<<<<-----Setting $server_ID delay to ${NEW_LEFT_DELAYS[0]}ms ${NEW_LEFT_DELAYS[1]}ms and ${NEW_LEFT_DELAYS[2]}ms------>>>>>\n"
    $SSH $server_ID "sudo tc qdisc del dev ens1 root;
                     sudo tc qdisc add dev ens1 root handle 1: htb;

                     sudo tc class add dev ens1 parent 1: classid 1:1 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 1 u32 flowid 1:1 match ip dst yyy.yyy.yyy.1/32;
                     sudo tc qdisc add dev ens1 parent 1:1 handle 10: netem delay ${NEW_LEFT_DELAYS[0]}ms limit 10000;

                     sudo tc class add dev ens1 parent 1: classid 1:2 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 2 u32 flowid 1:2  match ip dst yyy.yyy.yyy.2/32;
                     sudo tc qdisc add dev ens1 parent 1:2 handle 20: netem delay ${NEW_LEFT_DELAYS[1]}ms limit 10000;

                     sudo tc class add dev ens1 parent 1: classid 1:3 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 3 u32 flowid 1:3  match ip dst yyy.yyy.yyy.3/32;
                     sudo tc qdisc add dev ens1 parent 1:3 handle 30: netem delay ${NEW_LEFT_DELAYS[2]}ms limit 10000


                    sudo tc qdisc del dev ens2 root;
                    sudo tc qdisc add dev ens2 root handle 1: htb;

                    sudo tc class add dev ens2 parent 1: classid 1:1 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 1 u32 flowid 1:1 match ip src yyy.yyy.yyy.1/32;
                    sudo tc qdisc add dev ens2 parent 1:1 handle 10: netem delay ${NEW_LEFT_DELAYS[0]}ms limit 10000;

                    sudo tc class add dev ens2 parent 1: classid 1:2 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 2 u32 flowid 1:2  match ip src yyy.yyy.yyy.2/32;
                    sudo tc qdisc add dev ens2 parent 1:2 handle 20: netem delay ${NEW_LEFT_DELAYS[1]}ms limit 10000;

                    sudo tc class add dev ens2 parent 1: classid 1:3 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 3 u32 flowid 1:3  match ip src yyy.yyy.yyy.3/32;
                    sudo tc qdisc add dev ens2 parent 1:3 handle 30: netem delay ${NEW_LEFT_DELAYS[2]}ms limit 10000 ;"

  elif [[ $server_ID == "NLE-R" ]] ; then
    echo -e "\n\t<<<<<-----Setting $server_ID delay to ${NEW_RIGHT_DELAYS[0]}ms ${NEW_RIGHT_DELAYS[1]}ms and ${NEW_RIGHT_DELAYS[2]}ms------>>>>>\n"
    $SSH $server_ID "sudo tc qdisc del dev ens1 root;
                     sudo tc qdisc add dev ens1 root handle 1: htb;

                     sudo tc class add dev ens1 parent 1: classid 1:1 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 1 u32 flowid 1:1 match ip dst xxx.xxx.xxx.1/32;
                     sudo tc qdisc add dev ens1 parent 1:1 handle 10: netem delay ${NEW_RIGHT_DELAYS[0]}ms limit 10000;

                     sudo tc class add dev ens1 parent 1: classid 1:2 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 2 u32 flowid 1:2  match ip dst xxx.xxx.xxx.2/32;
                     sudo tc qdisc add dev ens1 parent 1:2 handle 20: netem delay ${NEW_RIGHT_DELAYS[1]}ms limit 10000;

                     sudo tc class add dev ens1 parent 1: classid 1:3 htb rate 5000mbit;
                     sudo tc filter add dev ens1 parent 1: protocol ip prio 3 u32 flowid 1:3  match ip dst xxx.xxx.xxx.3/32;
                     sudo tc qdisc add dev ens1 parent 1:3 handle 30: netem delay ${NEW_RIGHT_DELAYS[2]}ms limit 10000


                    sudo tc qdisc del dev ens2 root;
                    sudo tc qdisc add dev ens2 root handle 1: htb;

                    sudo tc class add dev ens2 parent 1: classid 1:1 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 1 u32 flowid 1:1 match ip src xxx.xxx.xxx.1/32;
                    sudo tc qdisc add dev ens2 parent 1:1 handle 10: netem delay ${NEW_RIGHT_DELAYS[0]}ms limit 10000;

                    sudo tc class add dev ens2 parent 1: classid 1:2 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 2 u32 flowid 1:2  match ip src xxx.xxx.xxx.2/32;
                    sudo tc qdisc add dev ens2 parent 1:2 handle 20: netem delay ${NEW_RIGHT_DELAYS[1]}ms limit 10000;

                    sudo tc class add dev ens2 parent 1: classid 1:3 htb rate 5000mbit;
                    sudo tc filter add dev ens2 parent 1: protocol ip prio 3 u32 flowid 1:3  match ip src xxx.xxx.xxx.3/32;
                    sudo tc qdisc add dev ens2 parent 1:3 handle 30: netem delay ${NEW_RIGHT_DELAYS[2]}ms limit 10000 ;"

  fi

}



#######################################
# To set the shaping rate on server1
# Arguments:
#   $1 shaping_rate
#   $2 queue
#######################################

set_shaping_rate () {

  local shaping_rate=$1
  local queue=$2
  local buffer=$(echo "scale=0; $shaping_rate/1000" | bc)
  echo -e "\n\t<<<<<----- Shaping rate=$((${shaping_rate}/1000000))Mbps and queue=$((${queue}/1000))KBytes on ens1 of Server 1 ----->>>>>\n"


  $SSH btlnck "sudo tc qdisc del dev ens1 root 2>/dev/null;
               sudo tc qdisc del dev ens2 root 2>/dev/null;
               sudo tc qdisc add dev ens1 root handle 1: prio;
               sudo tc qdisc add dev ens1 parent 1:3 handle 30: tbf rate ${shaping_rate} buffer ${buffer} limit ${queue} mtu 1500;
               sudo tc filter add dev ens1 protocol ip parent 1:0 prio 1 u32 match ip dst yyy.yyy.0.0/16 flowid 1:3;"

}


#######################################
# To Disable TSO on servers
# Arguments:
#   $1 server_ID
#   $2 interface
#######################################

disable_TSO (){
  local server_ID=$1
  local interface=$2
  $SSH $server_ID "sudo ethtool -K ${interface} tso off gso off gro off 2>/dev/null ;"
}

#######################################
# To set the harpoon sessions on server side
#######################################

setup_harpoon_servers (){
  server_sessions=$((${SESSIONS}*10))
  echo -e "\n\t<<<<<----- Setting Harpoon sessions:$SESSIONS, server side sessions:$server_sessions ----->>>>>\n"
  for flow in "${FLOWS[@]}"; do
    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )

    echo -e "\t<<<<<----- Setting $server_sessions sessions on server $src_server ----->>>>>"
    $SSH "${src_server}" "sed -i 's/<active_sessions>.*/<active_sessions> '"${server_sessions}"' <\/active_sessions>/g' /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_server.xml;"
    $SSH "${src_server}" "sed -i -E '\#port=\"1234\"# s#ipv4=\"[0-9./]+\"#ipv4=\"${src}/32\"#' /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_server.xml;"

  done

}

#######################################
# To set the harpoon sessions on client side
#######################################

setup_harpoon_clients (){
  for flow in "${FLOWS[@]}"; do

    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
    dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

    if [[ "${dst_server}" == "srv7" ]]; then
      echo -e "\t<<<<<----- Setting $SESSIONS sessions for $flow on client between src:$src_server($src) dst:$dst_server($dst)----->>>>>"
      $SSH "${dst_server}" "sed -i 's/<active_sessions>.*/<active_sessions> '"${SESSIONS}"' <\/active_sessions>/g' /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client.xml;"
      $SSH "${dst_server}" "cp /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client.xml /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_${flow}.xml; wait;"
      $SSH "${dst_server}" "sed -i -E '\#port=\"1234\"# s#ipv4=\"[0-9./]+\"#ipv4=\"${src}/32\"#' /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_${flow}.xml; wait;"
      $SSH "${dst_server}" "sed -i -E '\#port=\"0\"# s#ipv4=\"[0-9./]+\"#ipv4=\"${dst}/32\"#' /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_${flow}.xml; wait;"
    fi
  done

}


#######################################
# To print delay per flow table
#######################################

print_delays () {
  delay_factor=$1
echo -e "
\t+---------------+---------------+---------+-----------+
\t|      SRC      |      DST      | FLOW_ID | RTT(ms)
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.1 | yyy.yyy.yyy.1 |    1    | $(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.2 | yyy.yyy.yyy.1 |    2    | $(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.3 | yyy.yyy.yyy.1 |    3    | $(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.1 | yyy.yyy.yyy.2 |    4    | $(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.2 | yyy.yyy.yyy.2 |    5    | $(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.3 | yyy.yyy.yyy.2 |    6    | $(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.1 | yyy.yyy.yyy.3 |    7    | $(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.2 | yyy.yyy.yyy.3 |    8    | $(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
\t| xxx.xxx.xxx.3 | yyy.yyy.yyy.3 |    9    | $(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms
\t+---------------+---------------+---------+-----------+
"
}
