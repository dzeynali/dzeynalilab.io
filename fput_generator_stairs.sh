source ./variables.sh
input_dir=$1

IFS=":" read -ra CONGESTIONS_values <<< "${CONGESTIONS[0]}"
if [ "${#FLOWS[@]}" -eq 1 ] && [ "${FLOWS[0]}" = "1" ]; then
    cca=${CONGESTIONS_values[0]}
elif [ "${#FLOWS[@]}" -eq 1 ] && [ "${FLOWS[0]}" = "2" ]; then
    cca=${CONGESTIONS_values[1]}
fi

for file in $input_dir/*receiver.tput; do
        cat $file | awk '{printf("%.3f %d %d %d\n",$1, $2, $3, $4)}' >> $input_dir/findex.tmp
done

awk '{a[$1]=a[$1] FS $3} END{for(i in a) print i a[i]}' $input_dir/findex.tmp | sort -n | awk '{x=0;y=0;for(i=2;i<=NF;i++){x=x+$i; y=$i*$i+y}; if(y==0) print $1,0; else print $1, x**2 / (y * (NF-1)) }' > $input_dir/${cca}.fput

rm $input_dir/findex.tmp
