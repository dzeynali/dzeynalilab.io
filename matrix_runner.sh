#!/bin/bash
flow1=5
flow2=5

# Generate and print a 5x5 matrix
for ((i = 1; i <= flow1; i++)); do
  for ((j = 1; j <= flow2; j++)); do
    sed -i 's/^matrix_dict\["1"\]="[0-9]\+"/matrix_dict["1"]="'${j}'"/' variables.sh
    sed -i 's/^matrix_dict\["2"\]="[0-9]\+"/matrix_dict["2"]="'${i}'"/' variables.sh

    bash run_all.sh > ${i}_BBRv1_vs_${j}_Cubic.log; wait
    
    mkdir ${i}_BBRv1_vs_${j}_Cubic; wait
    mv cubic_bbr1_* ${i}_BBRv1_vs_${j}_Cubic; wait
    mv ${i}_BBRv1_vs_${j}_Cubic.log ${i}_BBRv1_vs_${j}_Cubic; wait
  done
done
