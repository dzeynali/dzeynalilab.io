source ./variables.sh
source ./setup_servers.sh

SSH="ssh -F ssh/config"
SCP="scp -F ssh/config"

TIME_NOW=$(date "+%d%m%Y_%H%M%S")

file_name=$1
delay_factor=$2

declare -A flows_dict # do not delete this line

#######################################
# Run harpoon Experiment -> This function will be called at the end of the file
#######################################

run_harpoon (){
  populate_flows_dict
  setup_harpoon_servers
  #setup_harpoon_clients
  exp_startup_harpoon $file_name
  warmup
  tc_queue
  #exp_run_harpoon
  exp_run_harpoon_hardcoded
  kill
  copy_files $file_name $TIME_NOW; wait
  delete_files $file_name $TIME_NOW; wait
  harpoon_throughput $file_name $TIME_NOW; wait
  harpoon_fct_zeek $file_name $TIME_NOW; wait
  harpoon_binned_fct $file_name $TIME_NOW; wait
}


#######################################
# Run Iperf Experiment -> This function will be called at the end of the file
#######################################

run_iperf (){
  populate_flows_dict
  exp_startup $file_name
  warmup
  tc_queue
  exp_run_iperf
  kill
  copy_files $file_name $TIME_NOW; wait
  delete_files $file_name $TIME_NOW; wait
  extract_flow_pcaps $file_name $TIME_NOW; wait
}

#######################################
# Run Iperf Matrix Experiment -> This function will be called at the end of the file
#######################################

run_iperf_matrix (){
  populate_flows_dict
  exp_startup $file_name
  warmup
  tc_queue
  exp_run_iperf_matrix
  kill
  copy_files $file_name $TIME_NOW; wait
  delete_files $file_name $TIME_NOW; wait
  extract_flow_pcaps $file_name $TIME_NOW; wait
}

#######################################
# Run Iperf stairs Experiment -> This function will be called at the end of the file
#######################################

run_iperf_stairs (){
  populate_flows_dict
  exp_startup $file_name
  warmup
  tc_queue
  tc_queue_stairs $FLOW_COUNT $GAP
  exp_run_iperf_staris $FLOW_COUNT $GAP
  kill
  copy_files $file_name $TIME_NOW; wait
  delete_files $file_name $TIME_NOW; wait
  extract_flow_pcaps_stairs $file_name $TIME_NOW; wait
}

#######################################
# Run Iperf inter-RTT/multi-rtt Experiment -> This function will be called at the end of the file
#######################################

run_iperf_inter_rtt (){
  populate_flows_dict
  exp_startup $file_name
  warmup
  tc_queue
  exp_run_iperf
  kill
  copy_files $file_name $TIME_NOW; wait
  delete_files $file_name $TIME_NOW; wait
  extract_flow_pcaps_inter_rtt $file_name $TIME_NOW; wait
}

#######################################
# To populate per flow RTT based on the factor
#######################################
populate_flows_dict () {
  flows_dict["1"]="xxx.xxx.xxx.1,yyy.yyy.yyy.1,$(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms,srv-L1,srv-R1"
  flows_dict["2"]="xxx.xxx.xxx.2,yyy.yyy.yyy.1,$(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms,srv-L2,srv-R1"
  flows_dict["3"]="xxx.xxx.xxx.3,yyy.yyy.yyy.1,$(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[0]})*2*${delay_factor}+2))ms,srv-L3,srv-R1"
  flows_dict["4"]="xxx.xxx.xxx.1,yyy.yyy.yyy.2,$(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms,srv-L1,srv-R2"
  flows_dict["5"]="xxx.xxx.xxx.2,yyy.yyy.yyy.2,$(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms,srv-L2,srv-R2"
  flows_dict["6"]="xxx.xxx.xxx.3,yyy.yyy.yyy.2,$(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[1]})*2*${delay_factor}+2))ms,srv-L3,srv-R2"
  flows_dict["7"]="xxx.xxx.xxx.1,yyy.yyy.yyy.3,$(((${LEFT_DELAYS[0]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms,srv-L1,srv-R3"
  flows_dict["8"]="xxx.xxx.xxx.2,yyy.yyy.yyy.3,$(((${LEFT_DELAYS[1]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms,srv-L2,srv-R3"
  flows_dict["9"]="xxx.xxx.xxx.3,yyy.yyy.yyy.3,$(((${LEFT_DELAYS[2]}+${RIGHT_DELAYS[2]})*2*${delay_factor}+2))ms,srv-L3,srv-R4"
}


#######################################
# Preparing servers IPERF and TCPDUMP
#######################################

exp_startup (){

  echo -e "\n\t <<<<<---- Setting up Iperf servers and tcpdumps ----->>>>>"

  $SSH srv-R1 "screen -dmS srv bash -c 'sudo taskset 0x800 iperf -s -B yyy.yyy.yyy.1' &"
  $SSH srv-R2 "screen -dmS srv bash -c 'sudo taskset 0x800 iperf -s -B yyy.yyy.yyy.2' &"
  $SSH srv-R3 "screen -dmS srv bash -c 'sudo taskset 0x800 iperf -s -B yyy.yyy.yyy.3' &"

  mkdir -p ${HOME_DIR}/${1}/${TIME_NOW}

  $SSH srv-L1 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L1.pcap" &
  $SSH srv-L2 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L2.pcap" &
  $SSH srv-L3 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L3.pcap" &

  $SSH srv-R1 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R1.pcap" &
  $SSH srv-R2 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R2.pcap" &
  $SSH srv-R3 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R3.pcap" &

  sleep 15; wait

}

#######################################
# Preparing servers IPERF and TCPDUMP
#######################################

exp_startup_harpoon (){

  echo -e "\n\t<<<<<---- Setting up Harpoon servers and tcpdumps ----->>>>>"

  $SSH srv-L1 "screen -d -m sudo taskset 0x2 /usr/local/harpoon/run_harpoon.sh -w ${DURATION} -s${SEED} -f /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_server.xml;" &
  $SSH srv-L2 "screen -d -m sudo taskset 0x2 /usr/local/harpoon/run_harpoon.sh -w ${DURATION} -s${SEED} -f /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_server.xml;" &
  $SSH srv-L3 "screen -d -m sudo taskset 0x800 /usr/local/harpoon/run_harpoon.sh -w ${DURATION} -s${SEED} -f /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_server.xml;" &

  mkdir -p ${HOME_DIR}/${1}/${TIME_NOW}

  $SSH srv-L1 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L1.pcap" &
  $SSH srv-L2 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L2.pcap" &
  $SSH srv-L3 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-L3.pcap" &

  $SSH srv-R1 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R1.pcap" &
  $SSH srv-R2 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R2.pcap" &
  $SSH srv-R3 "mkdir -p ~/${1}/${TIME_NOW}; screen -d -m sudo taskset -c 9-10 tcpdump -U -i enp0s1  -j adapter_unsynced --time-stamp-precision=nano  --immediate-mode -s 96  -w ~/${1}/${TIME_NOW}/${1}_srv-R3.pcap" &

  sleep 15; wait

}

#######################################
# Start TC queue log
#######################################

tc_queue () {
  echo -e "\n\t<<<<<----- Logging queue on the bottleneck server interface enp0s1 for ${DURATION} seconds  ----->>>>>"
  $SSH btlnck "./tc_queue_occ.sh enp0s1 ${DURATION} &" &
}

#######################################
# Start TC for stairs
#######################################

tc_queue_stairs () {
  flowCount=$1
  gap=$2
  duration=$(( ((2 * $flowCount) - 1 ) * $gap))
  echo -e "\n\t<<<<<----- Logging queue on the bottleneck server interface enp0s1 for ${DURATION} seconds  ----->>>>>"
  $SSH btlnck "./tc_queue_occ.sh enp0s1 ${DURATION} &" &
}

#######################################
# Warmup with pings
#######################################

warmup () {

  echo -e "\n\t<<<<<----- Warming up ----->>>>>"
 for flow in "${FLOWS[@]}"; do
   src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
   dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

   echo -e "\n\t<<<<<----- Warming up for ${flow} between ${src_server} $src and ${dst_server} $dst----->>>>>"

   $SSH "${src_server}" "ping ${dst} -I ${src} -i 0.2 -c 20 -q"; wait
 done
 sleep 5;
}

#######################################
# Killing Iperfs, TCPdumps and Screens
#######################################

kill (){

  echo -e "\n\t<<<<<----- Killing the Iperfs and Tcpdump ----->>>>>"
  $SSH srv-L1 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &
  $SSH srv-L2 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &
  $SSH srv-L3 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &
  $SSH srv-R1 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &
  $SSH srv-R2 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &
  $SSH srv-R3 "sudo pkill -9 -x iperf; sudo pkill -9 -x harpoon;" &

  sleep 1;

  $SSH srv-L1 "sudo pkill -9 -x screen" &
  $SSH srv-L2 "sudo pkill -9 -x screen" &
  $SSH srv-L3 "sudo pkill -9 -x screen" &
  $SSH srv-R1 "sudo pkill -9 -x screen" &
  $SSH srv-R2 "sudo pkill -9 -x screen" &
  $SSH srv-R3 "sudo pkill -9 -x screen" &

  #+++   Wipe old screens   +++++
  $SSH srv-L1 "screen -wipe >/dev/null 2>&1"
  $SSH srv-L2 "screen -wipe >/dev/null 2>&1"
  $SSH srv-L3 "screen -wipe >/dev/null 2>&1"
  $SSH srv-R1 "screen -wipe >/dev/null 2>&1"
  $SSH srv-R2 "screen -wipe >/dev/null 2>&1"
  $SSH srv-R3 "screen -wipe >/dev/null 2>&1"

}

#######################################
# Running the harpoon experiment for all flows
#######################################

exp_run_harpoon (){

  for flow in "${FLOWS[@]}"; do
    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
    dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )
    echo -e "\n\t<<<<<----- Running flow ${flow} between ${src_server} $src and ${dst_server} $dst----->>>>>"
    $SSH "${dst_server}" "screen -d -m sudo taskset 0x100 /usr/local/harpoon/run_harpoon.sh -w ${DURATION} ${SEED} -f ~/harpoon/distributions/${DISTRIBUTION}_client_${flow}.xml" &
  done
  sleep $((${DURATION}+5))

}

#######################################
# Running the Iperf experiment for all flows
#######################################

exp_run_iperf (){

 for flow in "${FLOWS[@]}"; do
   src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
   dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

    for ((i = 1; i <= PARALLEL; i++)); do
      echo -e "\n\t<<<<<----- Running flow ${flow}-${i} between ${src_server} $src and ${dst_server} $dst----->>>>>"
      $SSH "${src_server}" "screen -d -m sudo taskset 0x800 iperf -c ${dst} -B ${src} -P 1 -t ${DURATION}" &
      sleep $START_GAP;
    done
 done

 sleep $((${DURATION}+5))
}

#######################################
# Running the Iperf for 1-5 vs 1-5 flows
#######################################

exp_run_iperf_matrix (){

for flow in "${FLOWS[@]}"; do
  src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
  dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
  src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
  dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )
  my_parallel="${matrix_dict[$flow]}"
  echo -e "\n\t<<<<<----- Running flow ${flow} with ${my_parallel}_Parallel flows between ${src_server} $src and ${dst_server} $dst----->>>>>"
  $SSH "${src_server}" "screen -d -m sudo taskset 0x2 iperf -c ${dst} -B ${src} -P ${my_parallel} -t ${DURATION}" &
done

 sleep $((${DURATION}+5))

}

#######################################
# Running the Iperf experiment for stairs experiment
# $1 #flowCount
# $2 startGap
#######################################

exp_run_iperf_staris (){
  flowCount=$1
  startGap=$2

  for flow in "${FLOWS[@]}"; do
    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
    dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

    for ((i = 1; i <= flowCount; i++)); do
      echo -e "\n\t<<<<<----- Running flow ${flow}-${i} between ${src_server} $src and ${dst_server} $dst----->>>>>"
        $SSH "${src_server}" "screen -dm bash -c 'timeout $(( ((2*$flowCount)-1-(($i-1)*2)) * $startGap )) sudo taskset 0x2 iperf -c ${dst} -B ${src} -P 1 -t $(( ((2*$flowCount)-1-(($i-1)*2)) * $startGap ))'" &
        sleep $startGap;
    done
    sleep $(($startGap * $flowCount));
  done

 sleep 10;

}

#######################################
# Gathering .Pcap files from all servers
#######################################

copy_files (){

  echo -e "\n\t<<<<<----- Cppying files to the management server ${HOME_DIR}/${filename}/${TIME_NOW} ----->>>>>"
  local filename=$1
  local time_now=$2
  for flow in "${FLOWS[@]}"; do
   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )
   $SCP $src_server:~/${filename}/${time_now}/*.* ${HOME_DIR}/${filename}/${TIME_NOW}/
   $SCP $dst_server:~/${filename}/${time_now}/*.* ${HOME_DIR}/${filename}/${TIME_NOW}/
  done
  echo -e "\n\t<<<<<----- Cppying files done! ----->>>>>"
}

#######################################
# Deleting the current experiment folder
# from all the servers except SRV2
#######################################

delete_files (){

  echo -e "\n\t<<<<<----- Deleting files on Server 4,7,8 ----->>>>>"
  local filename=$1
  local time_now=$2

  $SSH srv-L1 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;"
  $SSH srv-L2 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;"
  $SSH srv-L3 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;"
  $SSH srv-R1 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;" 
  $SSH srv-R2 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;" 
  $SSH srv-R3 "rm -rf ~/${filename}/${time_now} >/dev/null 2>&1; rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;  "
  rm /home/bbrEval/harpoon/distributions/${DISTRIBUTION}_client_*.xml >/dev/null 2>&1;
  echo -e "\n\t<<<<<----- Deleting files done! ----->>>>>"
}


#######################################
# Extracting each flow from the main pcap file for Iperf experiment
#######################################

extract_flow_pcaps (){
 local filename=$1
 local time_now=$2
 for flow in "${FLOWS[@]}"; do
   src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
   dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
   rtt=$(echo "${flows_dict[$flow]}" | cut -d "," -f 3 )

   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

   cca=$($SSH ${src_server} "sudo sysctl net.ipv4.tcp_congestion_control" | awk -F '= ' '{print $2}')

   if [[ ${src_server} == "xxx" ]]; then # If your server has the BBRv3 kernel, then bbr=bbrv3 and bbr1=bbrv1 else if the server is bbrv2 kernel it means bbr1=bbrv1 and bbr2=bbrv2
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr3"
     fi
   elif [[ ${cca} == "bbr" ]]; then
     cca="bbr1"
   fi

  if [[ ${src_server} == "yyy" ]]; then
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr1"
     fi
  elif [[ ${cca} == "bbr2" ]]; then
     cca="bbr2"
  fi

   throughput_delay=$(echo $filename | cut -d "_" -f 4-)

   ports_filter_expression="tcp[tcpflags] & (tcp-syn) != 0 && tcp[tcpflags] & (tcp-ack) != 1 && (src $src and dst $dst)"
   ports=$(tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap -nn "$ports_filter_expression" 2>/dev/null \
    | awk '{print $3}' \
    | sed 's/://' \
    | awk -F '.' '{print $5}' | uniq)

    prt_counter=1
    for port in $ports; do
      filter_expression="((src $src and dst $dst) or (src $dst and dst $src)) && port $port "
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_sender.pcap 2>/dev/null
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_receiver.pcap  2>/dev/null
      echo -e "\n\t<<<<<----- Extracting sender and receiver pcaps for flow ${flow}-${prt_counter} between ${src_server} $src and ${dst_server} $dst with RTT=$rtt----->>>>>"
      bash tput_generator_stairs.sh ${HOME_DIR}/${filename}/${TIME_NOW}/ $src_server $dst_server "flow${flow}_${prt_counter}"  # for multiRTT enable this ->

      ((prt_counter++))
    done
 done

 #bash tput_generator.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
 cp *.py ${HOME_DIR}/${filename}/${TIME_NOW}/
 cp variables.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
 cd ${HOME_DIR}/${filename}/${TIME_NOW}/
 python3 throughput.py
 sudo rm *.py variables.sh *_srv*.pcap
 sudo rm -rf __pycache__
 cd -


}


#######################################
# Extracting each flow from the main pcap file for inter RTT experiments
#######################################

extract_flow_pcaps_inter_rtt (){
 local filename=$1
 local time_now=$2
 for flow in "${FLOWS[@]}"; do
   src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
   dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
   rtt=$(echo "${flows_dict[$flow]}" | cut -d "," -f 3 )

   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

   cca=$($SSH ${src_server} "sudo sysctl net.ipv4.tcp_congestion_control" | awk -F '= ' '{print $2}')

   if [[ ${src_server} == "xxx" ]]; then # If your server has the BBRv3 kernel, then bbr=bbrv3 and bbr1=bbrv1 else if the server is bbrv2 kernel it means bbr1=bbrv1 and bbr2=bbrv2
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr3"
     fi
   elif [[ ${cca} == "bbr" ]]; then
     cca="bbr1"
   fi

  if [[ ${src_server} == "yyy" ]]; then
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr1"
     fi
  elif [[ ${cca} == "bbr2" ]]; then
     cca="bbr2"
  fi

   throughput_delay=$(echo $filename | cut -d "_" -f 4-)

   ports_filter_expression="tcp[tcpflags] & (tcp-syn) != 0 && tcp[tcpflags] & (tcp-ack) != 1 && (src $src and dst $dst)"
   ports=$(tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap -nn "$ports_filter_expression" 2>/dev/null \
    | awk '{print $3}' \
    | sed 's/://' \
    | awk -F '.' '{print $5}' | uniq)

    prt_counter=1
    for port in $ports; do
      filter_expression="((src $src and dst $dst) or (src $dst and dst $src)) && port $port "
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_sender.pcap 2>/dev/null
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_receiver.pcap  2>/dev/null
      echo -e "\n\t<<<<<----- Extracting sender and receiver pcaps for flow ${flow}-${prt_counter} between ${src_server} $src and ${dst_server} $dst with RTT=$rtt----->>>>>"
      bash tput_generator_stairs.sh ${HOME_DIR}/${filename}/${TIME_NOW}/ $src_server $dst_server "flow${flow}_${prt_counter}"  # for multiRTT enable this ->

      ((prt_counter++))
    done
 done

 cp *.py ${HOME_DIR}/${filename}/${TIME_NOW}/
 cp variables.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
 cd ${HOME_DIR}/${filename}/${TIME_NOW}/
 python3 throughput_inter_rtt.py
 sudo rm *.py variables.sh *_srv*.pcap
 sudo rm -rf __pycache__
 cd -


}

#######################################
# Extracting each flow from the main pcap file for stairs experiment
#######################################

extract_flow_pcaps_stairs (){
 local filename=$1
 local time_now=$2
 for flow in "${FLOWS[@]}"; do
   src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
   dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
   rtt=$(echo "${flows_dict[$flow]}" | cut -d "," -f 3 )

   src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
   dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

   cca=$($SSH ${src_server} "sudo sysctl net.ipv4.tcp_congestion_control" | awk -F '= ' '{print $2}')

   if [[ ${src_server} == "xxx" ]]; then # If your server has the BBRv3 kernel, then bbr=bbrv3 and bbr1=bbrv1 else if the server is bbrv2 kernel it means bbr1=bbrv1 and bbr2=bbrv2
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr3"
     fi
   elif [[ ${cca} == "bbr" ]]; then
     cca="bbr1"
   fi

  if [[ ${src_server} == "yyy" ]]; then
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr1"
     fi
  elif [[ ${cca} == "bbr2" ]]; then
     cca="bbr2"
  fi

   throughput_delay=$(echo $filename | cut -d "_" -f 4-)

   ports_filter_expression="tcp[tcpflags] & (tcp-syn) != 0 && tcp[tcpflags] & (tcp-ack) != 1 && (src $src and dst $dst)"
   ports=$(tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap -nn "$ports_filter_expression" 2>/dev/null \
    | awk '{print $3}' \
    | sed 's/://' \
    | awk -F '.' '{print $5}' | uniq )

    prt_counter=1
    for port in $ports; do
      filter_expression="((src $src and dst $dst) or (src $dst and dst $src)) && port $port "
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_sender.pcap 2>/dev/null
      tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${prt_counter}_${cca}_${rtt}_${throughput_delay}_receiver.pcap  2>/dev/null
      echo -e "\n\t<<<<<----- Extracting sender and receiver pcaps for flow ${flow}-${prt_counter} between ${src_server} $src and ${dst_server} $dst with RTT=$rtt----->>>>>"
      bash tput_generator_stairs.sh ${HOME_DIR}/${filename}/${TIME_NOW}/ $src_server $dst_server "flow${flow}_${prt_counter}"
      ((prt_counter++))
    done

 done

 bash fput_generator_stairs.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
 cp *.py ${HOME_DIR}/${filename}/${TIME_NOW}/
 cp variables.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
 cd ${HOME_DIR}/${filename}/${TIME_NOW}/
 python3 throughput.py
 python3 fairness.py
 sudo rm *.py variables.sh 
 sudo rm -rf __pycache__
 cd -

}


#######################################
# To get the bins of binned FCT
#######################################

LOG_BINNING() {
  folder=$1
  rm -f forBinning.bin
  for file in $folder/*.fct; do
    head $file | awk '{if ($1=="tcp") print $8}' >> $folder/forBinning.bin
    tail $file | awk '{if ($1=="tcp") print $8}' >> $folder/forBinning.bin
  done
  local numbers=( $(cat $folder/forBinning.bin ) )

  # Sort the numbers in ascending order
  sorted_numbers=( $(echo "${numbers[@]}" | tr ' ' '\n' | sort -g) )

  # Determine the logarithmic range
  log_min=$(echo "${sorted_numbers[0]}" | awk '{print log($1)}')
  log_max=$(echo "${sorted_numbers[-1]}" | awk '{print log($1)}')

  # Determine the bin boundaries
  bin_boundaries=()
  current_boundary=$log_min
  log_range=$(echo "$log_max - $log_min" | bc -l)
  bin_size=$(echo "$log_range / $BIN_COUNT" | bc -l)
  for ((i=0; i<BIN_COUNT; i++))
  do
      current_boundary=$(echo "$current_boundary + $bin_size" | bc -l)
      bin_boundaries+=($(echo "scale=0; e($current_boundary)" | bc -l))
  done

  # Return the array of bin boundaries
  bin_boundaries[-1]=${sorted_numbers[-1]}
  AGG=("${bin_boundaries[@]}")
  rm -f $folder/forBinning.bin
  echo "${AGG[*]}"
}

#######################################
# Extracting FCTs with ZEEK
#######################################
harpoon_fct_zeek(){
  echo -e "\n\t<<<<<----- Extracting FCTs from .pcap files ----->>>>>"
  local filename=$1
  local time_now=$2
  for flow in "${FLOWS[@]}"; do
    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
    dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
    rtt=$(echo "${flows_dict[$flow]}" | cut -d "," -f 3 )

    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

    cca=$($SSH ${src_server} "sudo sysctl net.ipv4.tcp_congestion_control" | awk -F '= ' '{print $2}')

   if [[ ${src_server} == "xxx" ]]; then # If your server has the BBRv3 kernel, then bbr=bbrv3 and bbr1=bbrv1 else if the server is bbrv2 kernel it means bbr1=bbrv1 and bbr2=bbrv2
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr3"
     fi
   elif [[ ${cca} == "bbr" ]]; then
     cca="bbr1"
   fi

  if [[ ${src_server} == "yyy" ]]; then
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr1"
     fi
  elif [[ ${cca} == "bbr2" ]]; then
     cca="bbr2"
  fi



    throughput_delay=$(echo $filename | cut -d "_" -f 4-)
    if [ ! -f "${HOME_DIR}/${filename}/${TIME_NOW}/${TIME_NOW}/${filename}_${dst_server}.zeek" ]; then
      /usr/local/zeek/bin/zeek -C -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.pcap 'Site::local_nets={192.168.0.0/16}' local ; wait
      rm capture_loss.log known_services.log notice.log weird.log stats.log known_hosts.log loaded_scripts.log packet_filter.log
      mv conn.log ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.zeek
    fi
    cat ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.zeek \
                         | /usr/local/zeek/bin/zeek-cut proto ts id.orig_h id.orig_p id.resp_h id.resp_p duration orig_bytes resp_bytes conn_state history orig_ip_bytes resp_ip_bytes \
                         | awk -v src="${src}" -v dst="${dst}" 'NR>1{if($1=="tcp" && $3==dst && $5==src) print }' \
                         | sort -n -k2 \
                         | awk 'NR==1{ts=$2}{print $1,$2-ts,$3,$4,$5,$6,$7,$8+$9,$10,$11,$12,$13}' \
                         | awk -v time_cut=0 '{if ($2 >= time_cut) print $0}' \
                         | awk 'NR==1 {ts=$2} {print $1,$2-ts,$1=$2="",$0}' \
                         | sed 's/  */ /g' | grep -v "-" \
                         | awk '{if ($8 > 0) print $0}' \
                         | sort -n -k8 > ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${cca}_${rtt}_${throughput_delay}_receiver.fct
    sed -i '1s/^/proto ts id.orig_h id.orig_p id.resp_h id.resp_p duration orig_bytes+resp_bytes conn_state history orig_ip_bytes resp_ip_bytes\n/' ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${cca}_${rtt}_${throughput_delay}_receiver.fct
  done
}


#######################################
# Extracting tput for harpoon
#######################################
harpoon_throughput(){
  echo -e "\n\t<<<<<----- Extracting tput from .pcap files ----->>>>>"
  local filename=$1
  local time_now=$2
  for flow in "${FLOWS[@]}"; do
    src=$(echo "${flows_dict[$flow]}" | cut -d "," -f 1 )
    dst=$(echo "${flows_dict[$flow]}" | cut -d "," -f 2 )
    rtt=$(echo "${flows_dict[$flow]}" | cut -d "," -f 3 )

    src_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 4 )
    dst_server=$(echo "${flows_dict[$flow]}" | cut -d "," -f 5 )

    cca=$($SSH ${src_server} "sudo sysctl net.ipv4.tcp_congestion_control" | awk -F '= ' '{print $2}')

   if [[ ${src_server} == "xxx" ]]; then # If your server has the BBRv3 kernel, then bbr=bbrv3 and bbr1=bbrv1 else if the server is bbrv2 kernel it means bbr1=bbrv1 and bbr2=bbrv2
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr3"
     fi
   elif [[ ${cca} == "bbr" ]]; then
     cca="bbr1"
   fi

  if [[ ${src_server} == "yyy" ]]; then
     if [[ ${cca} == "bbr" ]]; then
       cca="bbr1"
     fi
  elif [[ ${cca} == "bbr2" ]]; then
     cca="bbr2"
  fi
    throughput_delay=$(echo $filename | cut -d "_" -f 4-)
    filter_expression="(tcp && (src $src and dst $dst) or (src $dst and dst $src))"
    tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${src_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${cca}_${rtt}_${throughput_delay}_sender.pcap 2>/dev/null
    tcpdump -r ${HOME_DIR}/${filename}/${TIME_NOW}/${filename}_${dst_server}.pcap  -nn "$filter_expression" -w ${HOME_DIR}/${filename}/${TIME_NOW}/flow${flow}_${cca}_${rtt}_${throughput_delay}_receiver.pcap  2>/dev/null
    echo -e "\n\t<<<<<----- Extracting sender and receiver pcaps for flow ${flow} between ${src_server} $src and ${dst_server} $dst with RTT=$rtt----->>>>>"
    bash tput_generator.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
  done

  cp *.py ${HOME_DIR}/${filename}/${TIME_NOW}/
  cp variables.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
  cd ${HOME_DIR}/${filename}/${TIME_NOW}/
  python3 throughput.py
  sudo rm *.py variables.sh #*_srv*.pcap
  sudo rm -rf __pycache__
  cd -

}

#######################################
# Extracting binned FCT
#######################################

harpoon_binned_fct(){
  echo -e "\n\t<<<<<----- calculating binned fct from .fct files -> .bfct ----->>>>>"
  local filename=$1
  local time_now=$2
  LOG_BINNING ${HOME_DIR}/${filename}/${TIME_NOW}

  for file in ${HOME_DIR}/${filename}/${TIME_NOW}/*.fct; do
    file_name=$(echo $(basename "$file") | cut -d "." -f 1)
    flow=$(echo $file_name | cut -d "_" -f 1)
    cca=$(echo $file_name | cut -d "_" -f 2)
    cca=${cca_map[${cca}]}
    rtt=$(echo $file_name | cut -d "_" -f 3)
    bw=$(echo $file_name | cut -d "_" -f 4)
    queue=$(echo $file_name | cut -d "_" -f 5)

    echo "proto ts id.orig_h id.orig_p id.resp_h id.resp_p duration orig_bytes+resp_bytes orig_ip_bytes resp_ip_bytes conn_state history bin flow cca rtt bw queue" > ${HOME_DIR}/${filename}/${TIME_NOW}/${file_name}.bfct

    prev_agg=0
    for agg in "${AGG[@]}"; do
      cat $file | awk  -v bin="${agg}" \
                       -v prevbin="${prev_agg}" \
                       -v flow="${flow}" \
                       -v cca="${cca}" \
                       -v rtt="${rtt}" \
                       -v bw="${bw}" \
                       -v queue="${queue}" \
                       '{if ((prevbin < $8) && ($8 <= bin)) { print $0, bin, flow, cca, rtt, bw, queue}}' >> ${HOME_DIR}/${filename}/${TIME_NOW}/${file_name}.bfct
      prev_agg=$agg
    done
  done

  cp *.py ${HOME_DIR}/${filename}/${TIME_NOW}/
  cp variables.sh ${HOME_DIR}/${filename}/${TIME_NOW}/
  cd ${HOME_DIR}/${filename}/${TIME_NOW}/
  cat *.fct | awk '{if ($8 > 4) print $8}' | grep -v "orig_bytes+resp_bytes" > flows_size.txt
  python3 cdf_plotter.py
  python3 binned_fct.py
  sudo rm *.py variables.sh #*_srv*.pcap
  sudo rm -rf __pycache__
  cd -

}

###################### Running the file ###############################
run_harpoon
#run_iperf
#run_iperf_inter_rtt
#run_iperf_matrix
#run_iperf_stairs
#######################################################################
