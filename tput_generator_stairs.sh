input_dir=$1
srcServer=$2
dstServer=$3
flowID=$4

source ./variables.sh

senderStartTime=$(ipsumdump -r  $input_dir/*${srcServer}.pcap -tlL -f "tcp" 2>/dev/null \
  | head -20 \
  | grep -v "!" \
  | awk 'NR==1 {printf "%.6f\n", $1}')

receiverStartTime=$(ipsumdump -r  $input_dir/*${dstServer}.pcap -tlL -f "tcp" 2>/dev/null \
  | head -20 \
  | grep -v "!" \
  | awk 'NR==1 {printf "%.6f\n", $1}')

#echo "senderStart=$senderStartTime receiverStart=$receiverStartTime"

for file in $input_dir/*"$flowID"*.pcap; do
  file_name=$(echo "${file}" | cut -d'.' -f 1  | rev | cut -d'/' -f1 | rev );
  if [[ $file == *"sender.pcap" ]]; then
    startTime=$senderStartTime
  else
    startTime=$receiverStartTime
  fi
  ipsumdump -r ${file} -tlL -f "tcp" 2>/dev/null \
   | grep -v "!" \
   | awk -v start="$startTime" '{if($3 > 0) print $1-start,$2,$3}' \
   | sort -n \
   | perl agg.pl ${GRAN} \
   | awk '{if ($3 > 0) print $0}' > $input_dir/${file_name}.tput 
done
