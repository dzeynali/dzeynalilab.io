# Promises and Potential of BBRv3
## Description

This is the main repository of the [Promises and Potential of BBRv3](https://inet-bbrv3eval.mpi-inf.mpg.de/) paper, presented at the [2024 Passive and Active Measurement Conference (PAM '24)](https://pam2024.cs.northwestern.edu/)

## What is the paper about?

The Bottleneck-Bandwidth and Round-trip (BBR) congestion control algorithm was introduced by Google in 2016. Unlike prior congestion-control algorithms (CCAs), BBR does not rely on signals that are weakly correlated with congestion (e.g., packet loss and tran- sient queue delay). Instead, it characterizes a path using two parameters, bottleneck bandwidth and round-trip propagation time, and is designed to converge with a high probability to Kleinrock’s optimal operating point.

This [paper](https://inet-bbrv3eval.mpi-inf.mpg.de/) focuses on characterizing the promises and potential of the third, and most recent, revision of BBR—introduced to the public in July 2023. We empirically evaluate BBRv3’s performance across a range of network scenarios, e.g., considering different buffer sizes, round-trip times, packet losses, and flow-size distributions.

## Requirements

We have used some tools and kernel versions to run, analyze, and plot the results. The following tools are required for this project:
- [BBRv2 kernel](https://github.com/google/bbr/blob/v2alpha/README.md)
- [BBRv3 kernel](https://github.com/google/bbr/blob/v3/README.md)
- [iPerf2](https://iperf.fr/iperf-doc.php)
- [ZEEK](https://zeek.org)
- [Harpoon](https://jsommers.github.io/harpoon/)
- [Ipsumdump](https://github.com/kohler/ipsumdump)

We use Debian 10 on all our servers and set up virtual machines using KVM on these servers to host different versions of BBR.

## Testbed

To set up experiments, we employed the following topology. This topology comprised six end hosts and could accommodate nine different flows between these endpoints. Each flow could possess its own delay (RTT) configuration.
The following topology represents our setup:
- Network Latency Emulator (NLE) nodes are required for setting up delays.
- [TC-Netem](https://man7.org/linux/man-pages/man8/tc-netem.8.html) node is responsible for the bottleneck setup.

![](https://inet-bbrv3eval.mpi-inf.mpg.de/assets/topology.png)


## Configuration

The first step is setting up the IPs for all servers and establishing connectivity. Replace all instances of `xxx.xxx.xxx.xxx` and `yyy.yyy.yyy.yyy`,... in all files with your desired IP addresses. 

To run an experiment, a user with sufficient access to execute commands on all servers, including the NLEs and TC, is required. The experiment setup will configure all the delays and initiate the experiments. You can set the servers IP addresses to run `SSH` between the servers in the `ssh/config` file.

copy the `tc_queue_occ.sh` to the TC server in order to monitor the queue occupancy.


Use `variables.sh` to define the experiment parameters. This file should be configured according to the scenarios you want to run.

`setup_servers.sh` script will prepare all the servers for a run, it starts with disabling TSO on all servers and bridging the interfaces for NLEs and TC and sets the configured delays on the NLE nodes as well as bottleneck bandwidth on TC. The congestion control algorithm setter is also in this file.


All the scenarios are running with the `run_experiment.sh` script which is called by the `run_all.sh`.
This includes starting pings before the experiment and running Iperf/Harpoon. Killing the processes and copying the files to the management server. Preperation of `.pcap` file per flow in the experiment is done by this script.
At the end of the `run_experiment.sh` you can chose what experiment to run.
`run_all.sh` script is responsible to run the experiment. 
If you want to run batch of experiment you can use `*_runner.sh` scripts.

Plotters are in the `plotters` directory. 


## Run experiments

After setting up desired variables you can run the experiments with the following command:
```
bash run_all.sh
```




