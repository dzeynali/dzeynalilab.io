sed -i 's/^FLOWS=.*/FLOWS=(1)/' variables.sh
sed -i 's/^CONGESTIONS=.*/CONGESTIONS=("bbr1:cubic:cubic")/' variables.sh
./run_all.sh > runbbr1.log  ; wait
mkdir stairs_bbr1
mv bbr1* stairs_bbr1; wait

sed -i 's/^FLOWS=.*/FLOWS=(1)/' variables.sh
sed -i 's/^CONGESTIONS=.*/CONGESTIONS=("bbr3:cubic:cubic")/' variables.sh
./run_all.sh > runbbr3.log  ; wait
mkdir stairs_bbr3
mv bbr3* stairs_bbr3; wait

sed -i 's/^FLOWS=.*/FLOWS=(1)/' variables.sh
sed -i 's/^CONGESTIONS=.*/CONGESTIONS=("cubic:cubic:cubic")/' variables.sh
./run_all.sh > runcubic.log  ; wait
mkdir stairs_cubic
mv cubic* stairs_cubic; wait

sed -i 's/^FLOWS=.*/FLOWS=(2)/' variables.sh
sed -i 's/^CONGESTIONS=.*/CONGESTIONS=("cubic:bbr2:cubic")/' variables.sh
./run_all.sh > runbbr2.log  ; wait
mkdir stairs_bbr2
mv bbr2* stairs_bbr2; wait