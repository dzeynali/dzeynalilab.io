input_dir=$1

source ./variables.sh

for file in $input_dir/*"flow"*.pcap; do

  file_name=$(echo "${file}" | cut -d'.' -f 1  | rev | cut -d'/' -f1 | rev );
  ipsumdump -r ${file} -tlL -f "tcp" 2>/dev/null \
   | grep -v "!" \
   | awk 'NR==1 {ts=$1} {if($3 > 0) print $1-ts,$2,$3}' \
   | sort -n \
   | perl agg.pl ${GRAN} \
   | awk '{if ($3 > 0) print $0}' > $input_dir/${file_name}.tput 

done
