./run_all.sh > cubic_sim.log  ; wait
mkdir run_cubic_sim
mv cubic* run_cubic_sim; wait

sed -i 's/^DURATION=.*/DURATION=365/' variables.sh
sed -i 's/^START_GAP=.*/START_GAP=15/' variables.sh
./run_all.sh > cubic_15sec.log  ; wait
mkdir run_cubic_15sec
mv cubic* run_cubic_15sec; wait
