#!/bin/bash
for ((i = 1; i <= 20; i++)); do
    sed -i 's/^SESSIONS=.*/SESSIONS="'${i}'"/' variables.sh
    bash run_all.sh > ${i}_sessions.log; wait
    mkdir ${i}_sessions; wait
    mv bbr3* ${i}_sessions; wait
    mv ${i}_sessions.log ${i}_sessions; wait
done

mkdir sessions_1_10_seed_1
mv *_sessions sessions_1_10_seed_1

sed -i 's/^SEED=.*/SEED=10/' variables.sh

for ((i = 1; i <= 20; i++)); do
    sed -i 's/^SESSIONS=.*/SESSIONS="'${i}'"/' variables.sh
    bash run_all.sh > ${i}_sessions.log; wait
    mkdir ${i}_sessions; wait
    mv bbrv3* ${i}_sessions; wait
    mv ${i}_sessions.log ${i}_sessions; wait
done

mkdir sessions_1_10_seed_10
mv *_sessions sessions_1_10_seed_10

sed -i 's/^SEED=.*/SEED=20/' variables.sh

for ((i = 1; i <= 20; i++)); do
    sed -i 's/^SESSIONS=.*/SESSIONS="'${i}'"/' variables.sh
    bash run_all.sh > ${i}_sessions.log; wait
    mkdir ${i}_sessions; wait
    mv bbrv3* ${i}_sessions; wait
    mv ${i}_sessions.log ${i}_sessions; wait
done

mkdir sessions_1_10_seed_20
mv *_sessions sessions_1_10_seed_20

sed -i 's/^SEED=.*/SEED=50/' variables.sh

for ((i = 1; i <= 20; i++)); do
    sed -i 's/^SESSIONS=.*/SESSIONS="'${i}'"/' variables.sh
    bash run_all.sh > ${i}_sessions.log; wait
    mkdir ${i}_sessions; wait
    mv bbrv3* ${i}_sessions; wait
    mv ${i}_sessions.log ${i}_sessions; wait
done

mkdir sessions_1_10_seed_50
mv *_sessions sessions_1_10_seed_50


