sed -i 's/^LEFT_DELAYS=.*/LEFT_DELAYS=(40 2 1)/' variables.sh
sed -i 's/^RIGHT_DELAYS=.*/RIGHT_DELAYS=(39 2 1)/' variables.sh
./run_all.sh > run10mscubic.log  ; wait
mkdir 160msvs10ms; wait
mv *cubic* 160msvs10ms; wait
mv 160msvs10ms 160msvs10ms; wait

sed -i 's/^LEFT_DELAYS=.*/LEFT_DELAYS=(40 5 1)/' variables.sh
sed -i 's/^RIGHT_DELAYS=.*/RIGHT_DELAYS=(39 4 1)/' variables.sh
./run_all.sh  > run20mscubic.log ; wait
mkdir 160msvs20ms; wait
mv *cubic* 160msvs20ms; wait
mv 160msvs20ms 160msvs20ms; wait


sed -i 's/^LEFT_DELAYS=.*/LEFT_DELAYS=(40 10 1)/' variables.sh
sed -i 's/^RIGHT_DELAYS=.*/RIGHT_DELAYS=(39 9 1)/' variables.sh
./run_all.sh > run40mscubic.log; wait
mkdir 160msvs40ms; wait
mv *cubic* 160msvs40ms; wait
mv 160msvs40ms 160msvs40ms; wait

sed -i 's/^LEFT_DELAYS=.*/LEFT_DELAYS=(40 20 1)/' variables.sh
sed -i 's/^RIGHT_DELAYS=.*/RIGHT_DELAYS=(39 19 1)/' variables.sh
./run_all.sh > run80mscubic.log; wait
mkdir 160msvs80ms; wait
mv *cubic* 160msvs80ms; wait
mv 160msvs80ms 160msvs80ms; wait